//
//  GameScene.swift
//  p06
//
//  Created by Abhishek on 4/6/17.
//  Copyright © 2017 Abhishek. All rights reserved.
//

import SpriteKit
import GameplayKit

var scoreNumber = 0
var randomImageNumber = arc4random()%4

class GameScene: SKScene {
    var index = 1
        //adding score
    
    let scoreLabel = SKLabelNode(fontNamed: "Pusab")
    //adding sound
    let playCorrectSoundEffect = SKAction.playSoundFileNamed("Correct.wav", waitForCompletion: false)
    //game over sound
    let playGameOverSoundEffect = SKAction.playSoundFileNamed("GameOverSound.wav", waitForCompletion: false)
    let playMusic = SKAction.playSoundFileNamed("music.wav", waitForCompletion: false)
   
    

    
    //creating universal game area ofr all devices
    let gameArea: CGRect
    
    override init(size: CGSize) {
        
        
        let maxAspectRatio: CGFloat = 16.0/9.0
        let playableWidth = size.height / maxAspectRatio
        let gameAreaMargin = (size.width - playableWidth)/2
        gameArea = CGRect(x: gameAreaMargin, y: 0, width: playableWidth, height: size.height)
        
        super.init(size: size)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //random number utility function
    //picks up the value between max and min number randomly to spawn the discs in game
    func random() -> CGFloat{
        return CGFloat(Float(arc4random())/0xFFFFFFFF)
    }
    
    func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    
    
    
     override func didMove(to view: SKView) {
        scoreNumber = 0
        //creating background
        let background = SKSpriteNode (imageNamed: "DiscsBackground")
        background.size = self.size
        background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        background.zPosition = 0
        self.addChild(background)
        
        //creating discs
        let disc = SKSpriteNode (imageNamed: "Disc2")
        disc.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        disc.zPosition = 2
        disc.name = "discObject"
        self.addChild(disc)
        
        
        //creating score label
        scoreLabel.fontSize = 250
        scoreLabel.text = "0"
        scoreLabel.fontColor = SKColor.white
        scoreLabel.zPosition = 1
        scoreLabel.position = CGPoint(x: self.size.width/2, y: self.size.height*0.85)
        self.addChild(scoreLabel)
        
        
    
    }
    //function to spawn the discs on screen
    func spawnNewDisc(){
        
        randomImageNumber = arc4random()%4
        randomImageNumber = randomImageNumber + 1
        let disc = SKSpriteNode(imageNamed: "Disc\(randomImageNumber)")
        disc.zPosition = 2
        disc.name = "discObject"
        //taking leftmost point in game area and rightmost point in game area to obtain random number between those two
        let randomX = random(min: gameArea.minX + disc.size.width/2, max: gameArea.maxX - disc.size.width/2)
        //similar to x random co-ordinate as mention above
        let randomY = random(min: gameArea.minY + disc.size.height/2, max: gameArea.maxY - disc.size.height/2)
        
        disc.position = CGPoint(x: randomX, y: randomY)
        
        self.addChild(disc)
        
        
        //shrink the discs
        disc.run(SKAction.sequence([
            SKAction.scale(to: 0, duration: 3),
            playGameOverSoundEffect,
            SKAction.run(runGameOver)
            
            ]))
        
    }
    //game over function
    func runGameOver(){
        //calling new scene i.e. game over
        let sceneToMoveTo = GameOverScene(size : self.size)
        //setting the size of scene similiar to this scene
        sceneToMoveTo.scaleMode = self.scaleMode
        //setting transition
        let sceneTransition = SKTransition.fade(withDuration: 0.2)
        self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            
            let positionOfTouch = touch.location(in: self)
            let tappedNode = atPoint(positionOfTouch)
            let nameOfTappedNode = tappedNode.name
            if nameOfTappedNode == "discObject"{
                
                tappedNode.name = "" //to avoide tapping on same disc multiple times.
                
                tappedNode.removeAllActions()//avoiding potential glitch of 0.1 sec
                //fading the node after tap
                tappedNode.run(SKAction.sequence([SKAction.fadeOut(withDuration: 0.1),SKAction.removeFromParent()]))
                //self.run(playMusic)
                self.run(playCorrectSoundEffect)
                
                
                
                
                
                //tappedNode.removeFromParent()
                
                
                
                //scoring based on disk number
                spawnNewDisc()
                //blue
                if randomImageNumber == 1 {
                    scoreNumber = scoreNumber + 1
                }
                //red
                if randomImageNumber == 2 {
                    scoreNumber = scoreNumber + 2
                }
                //white
                if randomImageNumber == 3 {
                    scoreNumber = scoreNumber + 3
                }
                //yellow
                if randomImageNumber == 4 {
                    scoreNumber = scoreNumber + 4
                }
                
                
                scoreLabel.text = "\(scoreNumber)"
                
                
                //adding new disc for each level 10 30 100 and so on..
                if scoreNumber == 10 || scoreNumber == 30 || scoreNumber == 50 || scoreNumber == 100 || scoreNumber == 150 || scoreNumber == 250 || scoreNumber == 400 || scoreNumber == 500 || scoreNumber == 750 || scoreNumber == 1000 {
                    
                    
                    spawnNewDisc()
                    
                }
                
            }
        }
    }
    
        
        
        
  
}

