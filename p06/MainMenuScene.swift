//
//  MainMenuScene.swift
//  p06
//
//  Created by Abhishek on 4/11/17.
//  Copyright © 2017 Abhishek. All rights reserved.
//

import Foundation
import SpriteKit


class MainMenuScene: SKScene {
    
   let playCorrectSoundEffect = SKAction.playSoundFileNamed("music.wav", waitForCompletion: false)
    let clickSound = SKAction.playSoundFileNamed("Click.wav", waitForCompletion: false)
    
    
    override func didMove(to view: SKView) {
        
       run(SKAction.sequence([playCorrectSoundEffect]))
        
        
        let background = SKSpriteNode(imageNamed: "DiscsBackground")
        background.size = self.size
        background.zPosition = 0
        background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        self.addChild(background)
        
       
        
        let gameTitleLabel2 = SKLabelNode(fontNamed: "Pusab")
        gameTitleLabel2.text = "Discs"
        gameTitleLabel2.fontSize = 250
        gameTitleLabel2.fontColor = SKColor.white
        gameTitleLabel2.position = CGPoint(x: self.size.width/2, y: self.size.height*0.6)
        gameTitleLabel2.zPosition = 1
        self.addChild(gameTitleLabel2)
        
        let howToPlayLabel = SKLabelNode(fontNamed: "Pusab")
        howToPlayLabel.text = "Tap the Discs Before They Vanish"
        howToPlayLabel.fontSize = 40
        howToPlayLabel.fontColor = SKColor.white
        howToPlayLabel.position = CGPoint(x: self.size.width/2, y: self.size.height*0.04)
        howToPlayLabel.zPosition = 1
        self.addChild(howToPlayLabel)
        
        let startLabel = SKLabelNode(fontNamed: "Pusab")
        startLabel.text = "Play"
        startLabel.fontSize = 150
        startLabel.fontColor = SKColor.white
        startLabel.position = CGPoint(x: self.size.width/2, y: self.size.height*0.35)
        startLabel.zPosition = 1
        startLabel.name = "startButton"
        self.addChild(startLabel)
        
        
    
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let pointOfTouch = touch.location(in: self)
            let tappedNode = atPoint(pointOfTouch)
            let tappedNodeName = tappedNode.name
            //restart button to restart the game
            if tappedNodeName == "startButton"{
                let sceneToMoveTo = GameScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                self.run(clickSound)
                let sceneTransition = SKTransition.fade(withDuration: 0.2)
                self.view!.presentScene(sceneToMoveTo, transition: sceneTransition)
                
                
            }
        }
    }
    
}
